## Considerations

There are a lot of open questions as we pursue this further and this document will exist to address some of those questions.

### Will this be more pitch competition or more hackathon?

First, before any hacking is done, there is definitely a pitch competition. The goal is great ideas. So the more ideas we can generate the better, and getting community involvement in pitching and helping process ideas is essential to success.

Some ideas may come from a group of people ready to build and they just need the backing of the sponsors. Some may come from a person who just has an idea and no way to develop it. That will be part of what comes next. The hackathon portion of this project comes after an idea is sourced and where there is need for community backing. But the goal is an open source tool that the community can hack on together.

### Will this be open source or backed by a company?

Open source is how we’re currently thinking about it, however if there is a commercial component that will help the project have greater reach and impact, we do not want to inherently limit that.

### Who can submit ideas?

The goal is great ideas. So the short answer is anyone. We want this to be as open and accessible as possible to the greater community.

### What do ideas need to look like to be considered?

The goal for this at first is great ideas. So we want the process to be as easy as possible so someone can submit something barely formed, with just a few lines, or something fully formed. Gitlab provides a platform for discussion, so if a very simple idea is submitted as an issue, discussion can take place on the issue itself to help clarify what category the submission satisfies, or what things need to be done specific to that idea. See [Submission Template](SUBMISSION_TEMPLATE.md) for a template to work off for a submission.

### Talk about What you app will do on April 30 and what you want it to do on Sept 30...

You need to bring development resources to get to April 30, other than promotion and support we cannot guarantee what we do after but tell us what would help the most. More development resources? Funding? Widest promotion possible and free distribution? What will make you idea have the greatest possible positive impact?